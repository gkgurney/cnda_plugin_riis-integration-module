package org.nrg.xnat.riis.dicom;

import com.google.common.collect.ImmutableSortedSet;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.id.DicomProjectIdentifier;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.riis.entities.RiisLoginRecord;
import org.nrg.xnat.riis.services.RiisService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedSet;

import static lombok.AccessLevel.PUBLIC;

/**
 * Custom DicomProjectIdentifier for the Riis Integration Module.
 * Determines the projectId associated with a Dicom file based on
 * the loginTime on the Riis System. (Stored in RiisLoginRecord)
 */
@Service
@Slf4j
@Getter(PUBLIC)
@Accessors(prefix = "_")
public final class RiisDicomProjectIdentifier implements DicomProjectIdentifier {
    /**
     * Class constructor - Sets the AE title for this instance.
     *
     * @param riisService The RIIS service.
     * @param aeTitle     The DICOM aeTitle this ProjectIdentifier is associated with.
     */
    public RiisDicomProjectIdentifier(final RiisService riisService, final String aeTitle) {
        _riisService = riisService;
        _aeTitle = aeTitle;
        _defaultUnassignedId = aeTitle + "_unassigned";
    }

    @Override
    public XnatProjectdata apply(final UserI user, final DicomObject dicomObject) {
        return XnatProjectdata.getProjectByIDorAlias(getProjectId(dicomObject), user, false);
    }

    /**
     * Gets the name of the project from the RiisService for the given dicom object.
     *
     * @param dicomObject The dicom object we want to know about.
     *
     * @return The name of the project, or an empty string if no project name was found.
     */
    private String getProjectId(final DicomObject dicomObject) {
        
    	String dicomDateStr=null;
	    //Retrieve the Dicom Date Strings from the Header. Remove any decimal portion of Time parameter.
	    if (! dicomObject.getString(Tag.StudyDate).equals(null) && ! dicomObject.getString(Tag.StudyDate).equals("") && ! dicomObject.getString(Tag.StudyTime).equals(null) && ! dicomObject.getString(Tag.StudyTime).equals("")) {
	    	String[] timeParse=dicomObject.getString(Tag.StudyTime).split("\\.");
	    	dicomDateStr = dicomObject.getString(Tag.StudyDate) + "_" + timeParse[0];
	    }

	    
	    // Must check to make sure that dicomDateStr gets set, otherwise, project sometimes assigned randomly
	    if (dicomDateStr != null ) {
	        try {
	            //Convert the dateStr into a Java Date object using SimpleDateFormat.parse()
	            final Date dicomDate = DATE_FORMAT.parse(dicomDateStr);
	
	            //Get the LoginRecord with the closest date/time to the date/time in the dicom file ("StudyDate_StudyTime")
	            log.debug("Calling getLoginRecordWithClosestDate({}, {})", dicomDateStr, getAeTitle());
	            final RiisLoginRecord loginRecord = getRiisService().getLoginRecordWithClosestDate(dicomDate, getAeTitle());
	
	            //If the LoginRecord is not null, get the projectId associated with this login record.
	            if (loginRecord != null) {
	                log.debug("getProjectId() returning {}", loginRecord.getProjectId());
	                return loginRecord.getProjectId();
	            }
	        } catch (Exception e) {
	            log.error("The Study Date/Study Time extracted from dicom was not in the expected format. Date received: \"{}\", expected format \"yyyyMMdd_HHmmss\"", dicomDateStr, e);
	        }
	    }

        log.info("No login record found for date {} and AE title {}, returning default unassigned ID {}", getDefaultUnassignedId());
        return getDefaultUnassignedId();
    }

    @Override
    public void reset() {
        // Nothing to do here since this is just set at initialization.
    }

    //Expected date format to be extracted from the dicom file "Tag.StudyDate_Tag.StudyTime"
    //DICOM standard states that the date format yyyy.MM.dd should be supported for backward compatibility.
    //Currently this module does not support this.
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss");

    private final SortedSet<Integer> _tags = ImmutableSortedSet.of();

    private final RiisService _riisService;  //The RiisService for retrieving the projectId.
    private final String      _aeTitle;      //The AE Title associated with this Identifier
    private final String      _defaultUnassignedId;

}
