package org.nrg.xnat.riis.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;

import javax.persistence.Entity;
import java.util.Date;

/**
 * RiisLoginRecord Hibernate entity.  This is represents one
 * row in the database.
 */
@Slf4j
@SuppressWarnings("deprecation")
@EqualsAndHashCode(callSuper = true)
@Entity
@Auditable
@Data
@Accessors(prefix = "_")
@AllArgsConstructor
@NoArgsConstructor
public final class RiisLoginRecord extends AbstractHibernateEntity {
    /**
     * Prints all private members to a formatted string.
     *
     * @return The string representation of the loginRecord.
     */
    @Override
    public String toString() {
        return "Project Id: " + getProjectId() + "\tLogin Time: " + getLoginTime() + "\tAE Title: " + getAeTitle();
    }

    private String _projectId;  //Id of the project
    private Date   _loginTime;  //The login time on the RIIS System.
    private String _aeTitle;    //The Dicom AE Title we are expecting the scan to come to.
}
