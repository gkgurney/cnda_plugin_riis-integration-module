package org.nrg.xnat.riis;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@XnatPlugin(value = "cndaRiis", name = "XNAT 1.7 RIIS module Plugin",
            entityPackages = "org.nrg.xnat.riis.entities",
            description = "This is the XNAT 1.7 RIIS module Plugin.",
            logConfigurationFile = "META-INF/resources/cndaRiis-logback.xml")
@ComponentScan({"org.nrg.xnat.riis.services.impl", "org.nrg.xnat.riis.daos"})
public class RiisModulePlugin {
}
